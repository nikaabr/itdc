<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;
class CategoryController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
    	$categories=Category::all();
    	//dd($categories);
    	return view('admin/categories/index',['categories'=>$categories]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories=Category::selectArray();
    	return view('admin/categories/create',['categories'=>$categories]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:20',
            'description' => 'required',
        ]);
        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        if( count(Category::find($request->parent))>0 )
        	$category->parent_id = $request->parent;
        $category->save();
        return redirect('admin/categories')->with('success', 1);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('show');    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
    	$categories=Category::selectArray(0,$category->id);
        return view('admin/categories/edit', ['category' => $category,'categories'=>$categories]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->validate($request, [
            'name' => 'required|max:20',
            'description' => 'required',
        ]);
        $category =Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;
        if( count(Category::find($request->parent))>0 )
        	$category->parent_id = $request->parent;
        $category->save();
        return redirect('admin/categories')->with('success', 1);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $news=Category::find($id)->news;
        foreach ($news as $item) {
            $item->category_id=NULL;
            $item->save();
        }
        Category::where('id', $id)->delete();
        return redirect('admin/categories');
    }
}