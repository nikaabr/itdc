<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\News;
class NewsController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($status='',$category='')

    {
        
        if (!empty($status)) {
            $news=News::where('status',$status)->get();
            $selectdeStatus=$status;
        }else{
            $news=News::all();
            $selectdeStatus='';
        }
        $categories=Category::selectArray(1);
    	return view('admin/news/index',['news'=>$news,'categories'=>$categories,'status'=>$selectdeStatus]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::selectArray(1);
        return view('admin/news/create',['categories'=>$categories]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image'=>'required'
            
        ]);
        $news=new News;
        if(!empty($request->image)){
            $destinationPath = public_path().'/uploads/images';
            $extensionImg = $request->image->getClientOriginalExtension();
            $fileNameImg = rand(11111,99999).'.'.$extensionImg;
            $request->image->move($destinationPath, $fileNameImg);
            $news->image=url('/uploads/images/'.$fileNameImg);
        }
        
        
        $news->name=$request->name;
        $news->category_id=$request->category;
        $news->status=$request->status;
        $news->content=$request->content;
        
        $news->save();
        return redirect('admin/news')->with('success', 1);

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=News::findOrFail($id);
        $categories=Category::selectArray(1);
        return view('admin/news/edit', ['news' => $news,'categories'=>$categories]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            //'image'=>'required',
            
        ]);
        $news=News::findOrFail($id);
        if(!empty($request->image)){
            $destinationPath = public_path().'/uploads/images';
            $extensionImg = $request->image->getClientOriginalExtension();
            $fileNameImg = rand(11111,99999).'.'.$extensionImg;
            $request->image->move($destinationPath, $fileNameImg);
            $news->image=url('/uploads/images/'.$fileNameImg);
        }
        
        
        $news->name=$request->name;
        $news->category_id=$request->category;
        $news->status=$request->status;
        $news->content=$request->content;
        
        $news->save();
        return redirect('admin/news')->with('success', 1);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        News::where('id', $id)->delete();
        return redirect('admin/news');
    }
}