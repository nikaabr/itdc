<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class News extends Model
{

	public function Category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function getStatus()
    {
    	if ($this->status=='active') {
    		return 'აქტიური';
    	}

    	return 'პასიური';
    }

}