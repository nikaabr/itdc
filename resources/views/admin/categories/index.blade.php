@extends('layouts.admin')
@push('meta')
<meta name="csrf-token" content="<?= csrf_token() ?>" />
<meta name="csrf-param" content="_token" />
@endpush
@section('title', 'კატეგორიები')

@section('maintitle', 'კატეგორიები')
@section('content')

<table class="table table-bordered table-striped table-hover">
    <thead>
      <tr>
        <th>სახელი</th>
        <th>აღწერა</th>
        <th>მშობელი</th>
        <th></th>
        <th></th>

      </tr>
    </thead>
    <tbody>
    @foreach($categories as $category)
      <tr>
        <td>{{$category->name}}</td>
        <td>{{$category->description}}</td>
        <td>
        	@if(isset($category->parent))
        		{{$category->parent->name}}
        	@endif
        </td>
        <td>
        	<a href="{{url('admin/categories/'.$category->id.'/edit')}}"><span class="fa fa-pencil"></span></a>

        </td>
        <td>
        	<a href="{{ route('categories.destroy',array($category->id)) }}" data-method="delete" rel="nofollow" data-confirm="Are you sure you want to delete this?"><span class="fa fa-remove"></span></a>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

@endsection
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ujs/1.2.2/rails.min.js"></script>

@endpush

