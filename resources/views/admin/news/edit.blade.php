@extends('layouts.admin')
@section('title', 'სიახლე')
@push('styles')
<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">

@endpush
@section('maintitle', 'სიახლის დამატება')
@section('content')
{!! Form::open([

		'url' => '/admin/news/'.$news->id,
		'method'=>'PUT',
		'class' => 'form-horizontal',
		'files' => true

	]) !!}

      <div class="form-group">
          <label class="select2_multiple col-md-4 col-xs-12 control-label" >კატეგორია</label>
          <div class="col-md-4 col-xs-12">   
        {!!
          Form::select(
            'category',
            $categories,
            old('category',$news->category_id),
            ['class'=>'form-control',]
            
          )
        !!}    
        </div>
      </div>

      <div class="form-group">
          <label class="select2_multiple col-md-4 col-xs-12 control-label" >კატეგორია</label>
          <div class="col-md-4 col-xs-12">   
        {!!
          Form::select(
            'status',
            [
            'active'=>'აქტიური',
             'pasive'=>'პასიური'
            ],
            old('status',$news->status),
            ['class'=>'form-control',]
            
          )
        !!}    
        </div>
      </div>

      <div class="form-group">
         <label class="control-label col-md-4 col-xs-12">სათაური</label>
          <div class="col-md-4 col-xs-12">
              <input  type="text" class="form-control" value="{{ old('name',$news->name) }}" name='name' placeholder="სახელი"
                data-validation="required" 
		 		data-validation-length="3-12" 
		 		data-validation-error-msg="სახელი აუცილებელია">
      		</div>

      </div>

      <div class="form-group">
         <label class="control-label col-md-4 col-xs-12">სურათი</label>
          <div class="col-md-4 col-xs-12">
	            <input type="file" class="form-control" name='image'>
	        </div>
      </div>

      


	  <div class="form-group">
         <label class="control-label col-md-4 col-xs-12">კონტენტი</label>
          <div class="col-md-4 col-xs-12">
          <textarea name="content" >{{ old('content',$news->content) }}</textarea>
          </div>
      </div>

  		
      
      <div class="form-group">
			  <label class="col-md-4 col-xs-12 control-label" for="singlebutton"></label>
			  <div class="col-md-4 col-xs-12">
			    <button type="submit" class="btn btn-success">რედაქტირება</button>
			  </div>
	</div>
  {!! Form::close() !!}

@endsection
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<script>
tinymce.init({
 selector:'textarea',
 height : 300,


 });

$.validate();

</script>
@endpush
